

local random = math.random
math.randomseed(os.time())

local grid = {w=10,h=10,mark=1,nomark=0}
grid.n=grid.w*grid.h

local mines=10
local world = {n=grid.w*grid.h, wanted=mines, marks=0, mark="#", free=nil}

local function empting(world)
	for i=1,world.n do world[i]=world.free end
	world.collisions = 0
	world.marks = 0
	return world
end

local function bestfill(world)
	if world.wanted > world.n then
		error("more wanted marks than world size")
	end
	local alias = {}
	local collisions = 0 -- to count the number of collision intercepted
	local size=world.n -- the number of cell to manage (reduced at each iteration)
	for _i=1,world.wanted do
		local n = random(1,size)
		--print("random picked", 1 .. "-" .. size, "=>", n, ": "..(world[n]or "_"))
		if alias[n] then collisions=collisions+1 end  -- debug/stats: keep track of number of collisions
		world[ alias[n] or n ] = world.mark           -- mark the target n (or use alias[n] instead, if exists)
		alias[n]=alias[size] or size                  -- remember the current n target is already use, jump to the size the next time (or remember to jump to alias[size] if we already have to jump)
		size=size-1                                   -- reduce the random range
	end
	world.collisions = collisions
	return world
end

local function stupidfill(world)
	if world.wanted > world.n then
		error("more wanted marks than world size")
	end
	local collisions = 0
	local size=world.n
	while world.marks < world.wanted do
		local n = random(1,size)
		--print("random picked", 1 .. "-" .. size, "=>", n, ": "..(world[n] or "_"))
		if world[n] == world.free then
			world[n]=world.mark
			size=size-1
			world.marks=world.marks+1
		else -- retry
			collisions = collisions+1
		end
	end
	world.collisions = collisions
	return world
end
local function show(world)
	for i=1,world.n do
		local v = world[i]
		io.stdout:write( (v and v or "_") )
	end
	print('')
end

if false then
	empting(world)
	stupidfill(world)
	print("collisions:", world.collisions)
	show(world)
end

empting(world)
bestfill(world)
print("collisions:", world.collisions)
show(world)

local function fillgridfromworld(grid, world)
	local w = grid.w
	local y,x
	for i=1,(world.n or #world) do
		y,x = math.floor(i/w)+1, i%w+1
		grid[y][x] = (world[i] and grid["mark"] or grid["nomark"])
	end
end
fillgridfromworld(grid, world)

--[[
for y=1, grid.h do
	grid[y] = grid[y] or {}
	for x=1, grid.w do
		grid[y][x] = 0
	end
end
]]--

for y=1,grid.h do
	for x=1,grid.w do
		io.stdout:write(tostring(grid[y][x]))
	end
	print('')
	--print(table.concat(grid[y],""))
end
os.exit(0)

local mines = {}

local function fill_retry(grid, totalmines)
	totalmines = totalmines or 10
	local marked = 0
	for y = 1, h do
		if marked == totalmines then break end
		for x = 1, w do
			if marked == totalmines then break end
			local rand = random(1,10)
			if rand == 10 and grid[y][x] == 0 then
				grid[y][x] = 1
				marked = marked + 1
			end
		end
	end
	return grid
end

	-- free = {1,2,3,4,5,6} max = #free
	-- r = rand(1,max) => rand(1,6) => r = 3
	-- free[3]=1
	-- free[max],free[r] = r,max
	-- -- free {1,2,6,4,5, 3}
	-- max = max -1
	-- r = rand(1,max) => rand(1,5) => r = 1
	-- -- free {5,2,6,4, 1,3}
	-- a la fin le tirage est free[max+1 a fin]


	-- Z = nil
	-- free = {Z,Z,Z,Z,Z,Z} max = #free
	-- r = rand(1,max) => rand(1,6) => r = 3
	-- free[3]=1
	-- free[max],free[r] = r,max
	-- -- free {Z,Z,6,Z,Z, 3}
	-- max = max -1
	-- r = rand(1,max) => rand(1,5) => r = 1
	-- -- free {5,Z,6,Z, 1,3}
	-- a la fin le tirage est free[max+1 a fin]
	-- si ca retombe sur le meme il faudra jumper a sa valeure


